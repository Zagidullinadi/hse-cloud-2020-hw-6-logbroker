aiohttp==3.7.3
fastapi==0.63.0
fastapi-utils==0.2.0
uvicorn==0.13.4
aiofile==3.4.0
