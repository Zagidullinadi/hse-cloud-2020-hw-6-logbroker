import csv
import json
import os
import ssl
from io import StringIO
import glob
import asyncio
import time

from aiohttp.client import ClientSession
from aiohttp.client_exceptions import ClientError
from aiofile import async_open
from fastapi import FastAPI, Request, Response
from fastapi_utils.tasks import repeat_every
from collections import defaultdict

CH_HOST = os.getenv('LOGBROKER_CH_HOST', 'localhost')
CH_USER = os.getenv('LOGBROKER_CH_USER')
CH_PASSWORD = os.getenv('LOGBROKER_CH_PASSWORD')
CH_PORT = int(os.getenv('LOGBROKER_CH_PORT', 8123))
CH_CERT_PATH = os.getenv('LOGBROKER_CH_CERT_PATH')


async def execute_query(query, data=None):
    url = f'http://{CH_HOST}:{CH_PORT}/'
    params = {
        'query': query.strip()
    }
    headers = {}
    if CH_USER is not None:
        headers['X-ClickHouse-User'] = CH_USER
        if CH_PASSWORD is not None:
            headers['X-ClickHouse-Key'] = CH_PASSWORD
    ssl_context = ssl.create_default_context(cafile=CH_CERT_PATH) if CH_CERT_PATH is not None else None

    async with ClientSession() as session:
        async with session.post(url, params=params, data=data, headers=headers, ssl=ssl_context) as resp:
            await resp.read()
            try:
                resp.raise_for_status()
                return resp, None
            except ClientError as e:
                return resp, {'error': str(e)}


app = FastAPI()
canceled = []

async def query_wrapper(query, data=None):
    res, err = await execute_query(query, data)
    if err is not None:
        return err
    return await res.text()


@app.get('/show_create_table')
async def show_create_table(table_name: str):
    resp = await query_wrapper(f'SHOW CREATE TABLE "{table_name}";')
    if isinstance(resp, str):
        return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
    return resp


@app.post('/write_log')
async def write_log(request: Request):
    body = await request.json()
    res = []
    for log_entry in body:
        table_name = log_entry['table_name']
        rows = log_entry['rows']
        if log_entry.get('format') == 'list':
            res.append(write_list(table_name, rows))
        elif log_entry.get('format') == 'json':
            res.append(write_json(table_name, rows))
        else:
            res.append({'error': f'unknown format {log_entry.get("format")}, you must use list or json'})
    return res


def write_list(table_name, rows):
    timestamp = int(time.time())
    filename = f'list_{timestamp}_{table_name}.log'
    with open(filename, 'a') as f:
        cwr = csv.writer(f, quoting=csv.QUOTE_ALL)
        cwr.writerows(rows)
    return filename


def write_json(table_name, rows):
    timestamp = int(time.time())
    filename = f'json_{timestamp}_{table_name}.log'
    with open(filename, 'a') as f:
        for row in rows:
            assert isinstance(row, dict)
            f.write(json.dumps(row))
            f.write('\n')
    return filename


@app.get('/healthcheck')
async def healthcheck():
    return Response(content='Ok', media_type='text/plain')


@app.get('/check_delivery')
async def check_delivery(query_id: str):
    global canceled
    res = []
    files = glob.glob('*_*_*.log')
    if query_id in canceled:
        res.append("Logs have been canceled")
    if query_id in files:
        res.append("Logs have not been recorded yet")
    else:
        res.append("Logs are recorded")
    return res


def get_file_info(filename):
    first_del = filename.find('_')
    second_del = filename.rfind('_')
    dot_del = filename.rfind('.')
    log_format = filename[:first_del]
    table_name = filename[second_del + 1:dot_del]
    return log_format, table_name


@app.on_event("startup")
@repeat_every(seconds=10)
async def send_files():
    global canceled
    files = glob.glob('*_*_*.log')
    data_dict = defaultdict(list)
    filenames = defaultdict(list)
    
    print(f"files: {files}", flush=True)
    for file in files:
        log_format, table_name = get_file_info(file)
        async with async_open(file, 'r') as f:
            data = await f.read()
        data_dict[(log_format, table_name)].append(data)
        filenames[(log_format, table_name)].append(file)
        print(f"data_dict: {data_dict}", flush=True)

    for key in data_dict:
        log_format, table_name = key[0], key[1]
        for data in data_dict[key]:
            if log_format == 'list':
                clickhouse_format = 'CSV'
            else:
                clickhouse_format = 'JSONEachRow'
            task = asyncio.create_task(query_wrapper(f'INSERT INTO \"{table_name}\" FORMAT {clickhouse_format}', data))
            res = await task
        if res == "":
            for file in filenames[key]:
                os.remove(file)
                print(f"File {file} removed", flush=True)
        else:
            canceled.append(f'{log_format}_{timestamp}_{table_name}.log')


